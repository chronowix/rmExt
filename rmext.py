import os
import time 

def rm_files(dir, exts):
    for root, dirs, files in os.walk(dir):
        for file in files:
            for ext in exts:
                if file.endswith(ext):
                    file_path = os.path.join(root,file)
                    os.remove(file_path)
                    print("Files removed")

path = "~/Téléchargements/"
extensions = [".sh", ".tgz", ".zip", ".tar", ".run", ".deb"]
time.sleep(2)
rm_files(path, extensions)
